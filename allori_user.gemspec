# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'allori_user/version'

Gem::Specification.new do |spec|
  spec.name          = "allori_user"
  spec.version       = AlloriUser::VERSION
  spec.authors       = ["Dave Burt"]
  spec.email         = ["dave.burt@allori.edu.au"]
  spec.description   = %q{Allori authentication model}
  spec.summary       = %q{Allori authentication model}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency "activesupport"
  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"
end
