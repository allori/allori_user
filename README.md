# AlloriUser

Allori authentication model

## Installation

Add this line to your application's Gemfile:

    gem 'allori_user', git: 'https://bitbucket.org/allori/allori_user.git'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install allori_user

## Usage

`AlloriUser.authenticate(username, password)` will return nil or an AlloriUser object with `email` and `name` methods which return strings.

`AlloriUser.hostname = 'auth.example.com'` will set the hostname requests are sent to. Do this once, before calling `authenticate`.

`AlloriUser.group = 'App User Group'` will set the group users must belong to. Do this once, before calling `authenticate`, or don't and let the auth server use its default.

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request

## Add Server to Auth Whitelist

Authentication is controlled by the server, [auth.allori.com.au](http://bitbucket.org/allori/auth.allori.com.au). See that project for the process for updating the whitelist.
