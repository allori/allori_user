require 'spec_helper'

require './lib/allori_user'

describe AlloriUser do
  describe "hostname" do
    it "defaults to auth.allori.com.au" do
      expect(AlloriUser.hostname).to eq("auth.allori.com.au")
    end
  end

  describe "hostname=" do
    it "changes the hostname" do
      begin
        old_hostname = AlloriUser.hostname
        AlloriUser.hostname = "custom.example.com"
        expect(AlloriUser.hostname).to eq("custom.example.com")
      ensure
        AlloriUser.hostname = old_hostname
      end
    end

    it "doesn't accept other URL components, only hostnames" do
      begin
        old_hostname = AlloriUser.hostname
        expect { AlloriUser.hostname = "custom.example.com:443" }.to raise_error
        expect { AlloriUser.hostname = "custom.example.com/path" }.to raise_error
        expect { AlloriUser.hostname = "https://custom.example.com/" }.to raise_error
      ensure
        AlloriUser.hostname = old_hostname
      end
    end
  end

  describe "group" do
    it "defaults to nil (i.e. determined by server)" do
      expect(AlloriUser.group).to eq(nil)
    end
  end

  describe "group=" do
    it "sets the group to authenticate against" do
      begin
        old_group = AlloriUser.group
        AlloriUser.group = "Test Group 1"
        expect(AlloriUser.group).to eq("Test Group 1")
      ensure
        AlloriUser.group = old_group
      end
    end

    it "treats blank strings as nil" do
      begin
        old_group = AlloriUser.group
        AlloriUser.group = " \t\n"
        expect(AlloriUser.group).to eq(nil)
      ensure
        AlloriUser.group = old_group
      end
    end
  end

  describe "#authenticate" do
    subject { AlloriUser.authenticate username, password }

    let(:username) { "username" }
    let(:password) { "password" }

    it "sends credentials to host" do
      expect(AlloriUser).to receive(:https_post).
        with("/", "username=#{username}&password=#{password}")
      subject
    end

    context "with symbols in password" do
      let(:password) { "a*&#?/: z" }

      it "escapes symbols in sent credentials" do
        encoded_password = "a%2A%26%23%3F%2F%3A%20z"
        expect(AlloriUser).to receive(:https_post).
          with("/", "username=#{username}&password=#{encoded_password}")
        subject
      end
    end

    context "with group set" do
      before { @old_group = AlloriUser.group; AlloriUser.group = 'App-Specific Group' }
      after { AlloriUser.group = @old_group }
      it "sends credentials and group to host" do
        expect(AlloriUser).to receive(:https_post).
          with("/", "username=#{username}&password=#{password}&group=App-Specific%20Group")
        subject
      end
    end

    context "with no credentials" do
      let(:username) { nil }
      let(:password) { nil }
      it "is nil" do
        expect(AlloriUser).to receive(:https_post).
          and_return Net::HTTPUnauthorized.new("1.0", "401", "Access denied")
        expect(subject).to be_nil
      end
    end

    context "with bad credentials" do
      it "is nil" do
        expect(AlloriUser).to receive(:https_post).
          and_return Net::HTTPUnauthorized.new("1.0", "401", "Access denied")
        expect(subject).to be_nil
      end
    end

    context "with good credentials" do
      let(:happy_response) do
        resp = Net::HTTPOK.new("1.0", "200", "OK").tap do |response|
          response.instance_variable_set "@body", happy_response_body
          response.instance_variable_set "@read", true
        end
      end
      let(:happy_response_body) { "<user><name>Bob</name><email>bob@example.com</email></user>" }

      before do
        expect(AlloriUser).to receive(:https_post).and_return happy_response
      end

      it "is an AlloriUser object" do
        expect(subject).to be_kind_of(AlloriUser)
      end

      it "has an email from the server's response" do
        expect(subject.email).to eq("bob@example.com")
      end

      it "has a name from the server's response" do
        expect(subject.name).to eq("Bob")
      end

      it "handles dynamic methods with #respond_to?" do
        expect(subject.respond_to?(:name)).to eq(true)
        expect(subject.respond_to?(:email)).to eq(true)
      end
    end
  end
end
