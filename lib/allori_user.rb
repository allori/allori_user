require "allori_user/version"

# Standard library
require 'ostruct'
require 'cgi'
require 'net/http'
require 'net/https'  # for Ruby 1.8.7
require 'erb'

# Gems
require 'active_support/core_ext/hash/conversions'

class AlloriUser
  HOSTNAME_PATTERN = /\A[A-Za-z0-9\-\.]+\Z/
  def initialize(hash)
    @struct = OpenStruct.new(hash)
  end

  def method_missing(meth, *args, &block)
    @struct.send(meth, *args, &block)
  end

  def respond_to_missing?(meth, include_private = false)
    @struct.respond_to?(meth) || super
  end

  def self.authenticate(login, password)
    resp = https_post "/",
      "username=#{u login}&password=#{u password}" + group_query_string_suffix
    new(Hash.from_xml(resp.body.to_s)["user"]) if resp.kind_of?(Net::HTTPSuccess)
  end

  def self.hostname
    @hostname
  end
  @hostname = "auth.allori.com.au"

  def self.hostname=(new_host)
    unless new_host =~ HOSTNAME_PATTERN
      raise ArgumentError, "invalid hostname format: #{new_host.inspect} does not match #{HOSTNAME_PATTERN}"
    end
    @hostname = new_host
  end

  def self.group_query_string_suffix
    group ? "&group=#{u group}" : ""
  end

  def self.group
    @group
  end

  def self.group=(new_group)
    @group = (new_group unless new_group.to_s.strip.empty?)
  end

  private

  def self.u(s)
    ERB::Util.url_encode s.to_s
  end

  def self.https_post(path, body)
    http = Net::HTTP.new(hostname, 443)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    http.post path, body, 'Content-Type' => 'application/x-www-form-urlencoded'
  end
end
